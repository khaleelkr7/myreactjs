import React, {Component} from 'react';
import './App.css';
import ReactTable from "react-table";
import "react-table/react-table.css"

class App extends Component {

  constructor(props){
    super(props);

    this.state={
      posts: []
    }
  }

  componentDidMount(){
    const url="https://jsonplaceholder.typicode.com/posts";
    fetch (url,{
      method:"GET"
    }).then(reponse=>reponse.json()).then(posts=>{
      this.setState({posts: posts})
    })
  }

  render(){
    const columns=[
      {
        Header: "User ID",
        accessor: "userId"
      },
      {
        Header: "ID",
        accessor: "id"
      },
      {
        Header: "Title",
        accessor: "title",
        filterable:false
      },
      {
        Header: "Body",
        accessor: "body",
        filterable:false
      },
      {
        Header: "Actions",
        Cell: props =>{
          return(
            <button style={{backgroundColor: "red", color: "#fefefe"}}
            onClick={()=>{
              console.log("props", props)
            }}
            >Delete</button>
          )
        },
        filterable:false
      },
    ]
    return (
      <ReactTable
      columns={columns}
      data={this.state.posts}
      filterable
      >

      </ReactTable>
    );
  }
}

  

export default App;
